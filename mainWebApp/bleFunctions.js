/* Global vars */
var temperatureValue = 'N/A';
var humidityValue = 'N/A';
var CO2LevelValue = 'N/A';

/* Current device */
var currentDevice = null;



/* Threshold variables adopted from online sources */
var temperatureUpperLimit = 24.5 //Centigrade attained from https://www.cielowigle.com/blog/ideal-room-temperature/
var temperatureLowerLimit = 20 //Centigrade

var humidityUpperLimit = 60 //Percent attained from https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4873430/#:~:text=The%20U.S.%20Environmental%20Protection%20Agency's,%25%20(U.S.%20EPA%202009).
var humidityLowerLimit = 40 //Percent

var CO2UpperLimit = 1000 //ppm attained from https://www.co2meter.com/blogs/news/7334762-indoor-air-quality-in-the-classroom
var CO2LowerLimit = 0 //ppm

/* Actionable items */
var temperatureUpperLimitWarning = "Low Temperature"
var temperatureLowerLimitWarning = "High Temperature"

var temperatureUpperLimitAction = ["Increase room temperature to at least " + temperatureLowerLimit.toString(), "Turn on fan", "Open Window"]
var temperatureLowerLimitAction = ["Reduce room temperature below " + temperatureLowerLimit.toString(), "Close Window"]

var humidityUpperLimitWarning = "Low Humidity"
var humidityLowerLimitWarning = "High Humidity"

var humidityUpperLimitAction = ["Turn on fan", "Open Window"]
var humidityLowerLimitAction = ["Use a humidifier", "Boil water"]

var CO2UpperLimitWarning = "Low CO2 Level"
var CO2LowerLimitWarning = "High CO2 Level"

var CO2UpperLimitAction = ["Open Windows", "Turn on Fan", "Open Doors"]
var CO2LowerLimitAction = []
    /**
     * Function attempts to connect to airtame device
     * @returns None
     */
async function connectAirtameDevice() {
    if ((currentDevice != null && currentDevice.gatt.connected)) return; //if already connected return
    await navigator.bluetooth.requestDevice({
            acceptAllDevices: true,
            optionalServices: ['environmental_sensing'] // Required to access service later. UUID 0000181a-0000-1000-8000-00805f9b34fb
        })
        .then(device => {
            // Human-readable name of the device.
            console.log(device.name);

            currentDevice = device;

            // Set up event listener for when device gets disconnected.
            device.addEventListener('gattserverdisconnected', handleAirtameDeviceServerDisconnected);

            // Attempts to connect to remote GATT Server. 00001800-0000-1000-8000-00805f9b34fb
            return device.gatt.connect();
        })
        .then(server => {
            // Getting environmental_sensing Service…
            return server.getPrimaryService('environmental_sensing');
        })
        .then(service => { //https://www.bluetooth.com/specifications/assigned-numbers/
            // Getting Temperature Characteristic…
            console.log('Getting Characteristics...');
            let queue = Promise.resolve();

            queue = queue.then(_ => service.getCharacteristics().then(characteristics => {
                console.log('> Service: ' + service.uuid);
                characteristics.forEach(characteristic => {
                    //console.log('>> Characteristic: ' + characteristic.uuid + characteristic.readValue());

                    if (characteristic.uuid.toLowerCase().includes("2a6e")) { //Temperature
                        console.log('>>Found Temperature Characteristic: ' + characteristic.uuid);

                        characteristic.startNotifications()
                            .then(temp => {
                                characteristic.addEventListener('characteristicvaluechanged',
                                    handleBleTemperatureValueChanged);
                                console.log('Added temp listener')
                                return characteristic.readValue();
                            }).then(value => {
                                temperatureValue = value.getUint8(0);
                                console.log(`temperatureValue is ${temperatureValue}`);
                                updateDashboard();
                            })

                    } else if (characteristic.uuid.toLowerCase().includes("2a6f")) { //Humidity
                        console.log('>>Found Humidity Characteristic: ' + characteristic.uuid);
                        characteristic.startNotifications()
                            .then(temp => {
                                // Set up event listener for when characteristic value changes.
                                characteristic.addEventListener('characteristicvaluechanged',
                                    handleBleHumidityValueChanged)
                                console.log('Added humidity listener')
                                return characteristic.readValue();
                            }).then(value => {
                                humidityValue = value.getUint8(0);
                                console.log(`humidityValue is ${humidityValue}`);
                                updateDashboard();
                            })

                    } else if (characteristic.uuid.toLowerCase().includes("2bd5")) { //CO2
                        /* Currently not working */
                        console.log('>>Found CO2 Characteristic: ' + characteristic.uuid);
                        characteristic.startNotifications()
                            .then(temp => {
                                // Set up event listener for when characteristic value changes.
                                characteristic.addEventListener('characteristicvaluechanged',
                                    handleBleCO2ValueChanged)
                                console.log('Added CO2 listener')
                                return characteristic.readValue();
                            }).then(value => {
                                CO2LevelValue = value.getUint8(0);
                                console.log(`CO2LevelValue value is ${CO2LevelValue}`);
                                updateDashboard();
                            })
                    }

                });
            }));
            //return queue;
            //Temperature-00002a6e-0000-1000-8000-00805f9b34fb Humidity-00002a6f-0000-1000-8000-00805f9b34fb CO2-00002bd5-0000-1000-8000-00805f9b34fb
        })

    .catch(error => { console.error(error); });

}


/**
 * Function attempts to disconnect device
 */
async function disconnectAirtameDevice() {
    if (currentDevice != null) {
        currentDevice.gatt.disconnect(); //Attempt disconnection
    }
    currentDevice = null;


    //reset Global vars
    temperatureValue = 'N/A';
    humidityValue = 'N/A';
    CO2LevelValue = 'N/A';


    //Update dashboard
    updateDashboard();
}

/** 
 * Function runs when the connect device button is clicked and alternates between connected and disconnected states
 */
async function changeAirtameDeviceConnectionStatus() {
    if ((currentDevice != null && currentDevice.gatt.connected)) {
        /*Attempt disconnection */
        console.log("Attempting disconnection")
        disconnectAirtameDevice();

    } else {
        /* Not yet connected so attempt connection */
        await connectAirtameDevice();
        if (currentDevice != null && currentDevice.gatt.connected) {
            console.log('Updating dasboard')
            updateDashboard();
        }
    }


}

/**
 * Function updates the page values and buttons
 */
function updateDashboard() {
    //Update values
    console.log("Updating dashboard")
    document.getElementById("htmlTemperature").innerHTML = temperatureValue.toString();
    document.getElementById("htmlHumidity").innerHTML = humidityValue.toString();
    document.getElementById("htmlCO2Level").innerHTML = CO2LevelValue.toString();

    //Update buttons
    updateConnectionStatusButton() //connection status
    updateConnectDeviceButton() //connect device buttion
}

function getTemperature() {
    console.log('Temperature Value is' + temperatureValue);
    //document.getElementById("htmlTemperature").innerHTML = temperatureValue.toString();
    document.getElementById("htmlTemperature").value = temperatureValue;
    return temperatureValue;
}

/**
 * Handles event when value is changed in device
 * @param {*} event 
 */
function handleBleCustomValueChanged(event) {
    const CustomValue = event.target.value.getUint8(0);
    temperatureValue = humidityValue = CO2LevelValue = CustomValue;
    console.log('Characteristic value  percentage is ' + CustomValue);
    updateDashboard();
}

function handleBleHumidityValueChanged(event) {
    const CustomValue = event.target.value.getUint8(0);
    humidityValue = CustomValue;
    console.log('Characteristic value  humidityValue is ' + CustomValue);
    updateDashboard();
}


function handleBleCO2ValueChanged(event) {
    const CustomValue = event.target.value.getUint8(0);
    CO2LevelValue = CustomValue;
    console.log('Characteristic value  CO2LevelValue is ' + CustomValue);
    updateDashboard();
}

function handleBleTemperatureValueChanged(event) {
    const CustomValue = event.target.value.getUint8(0);
    temperatureValue = CustomValue;
    console.log('Characteristic value  temperatureValue is ' + CustomValue);
    updateDashboard();
}


/**
 * Listener for disconnected events
 * @param {*} event 
 */
function handleAirtameDeviceServerDisconnected(event) {
    disconnectAirtameDevice();
    const device = event.target;
    console.log(`Device : ${device.name} is disconnected.`);
    //window.alert(`Device : ${device.name}  disconnected.`);
}

/**
 * Updates the connection status button
 */
function updateConnectionStatusButton() {
    /*
        In CSS u-btn-1 is for disconnected
        u-btn-2 is for connected State

        Tips from https://stackoverflow.com/questions/195951/how-can-i-change-an-elements-class-with-javascript
    */
    if ((currentDevice != null && currentDevice.gatt.connected)) {
        document.getElementById("htmlConnectionStatusButton").className = "u-border-none u-btn u-btn-round u-button-style u-gradient u-none u-radius-50 u-text-body-alt-color u-btn-2"
        document.getElementById("htmlConnectionStatusButton").innerHTML = 'Connected'
    } else {
        document.getElementById("htmlConnectionStatusButton").className = "u-border-none u-btn u-btn-round u-button-style u-gradient u-none u-radius-50 u-text-body-alt-color u-btn-1"
        document.getElementById("htmlConnectionStatusButton").innerHTML = 'Disconnected'
    }
}

/**
 * Updates the connect device button on html
 */
function updateConnectDeviceButton() {
    /*
        In CSS u-palette-2-base is for disconnected (red)
        u-palette-1-base is for connected State (blue)
        Tips from https://stackoverflow.com/questions/195951/how-can-i-change-an-elements-class-with-javascript

    */
    if ((currentDevice != null && currentDevice.gatt.connected)) {
        //Currently connected so display disconnect device option
        document.getElementById("htmlConnectDeviceButton").className = "u-border-none u-btn u-btn-round u-button-style u-palette-2-base u-radius-36 u-text-body-alt-color u-btn-1"
        document.getElementById("htmlConnectDeviceButton").innerHTML = 'DISCONNECT DEVICE'
    } else {
        //Currently disconnected so display connect device option
        document.getElementById("htmlConnectDeviceButton").className = "u-border-none u-btn u-btn-round u-button-style u-palette-1-base u-radius-36 u-text-body-alt-color u-btn-1"
        document.getElementById("htmlConnectDeviceButton").innerHTML = 'CONNECT DEVICE'
    }
}

/**
 * Function attempts to connect to airtame device
 * @returns None
 */
async function connectAirtameDeviceTestingFunction() {
    if ((currentDevice != null && currentDevice.gatt.connected)) return; //if already connected return

    await navigator.bluetooth.requestDevice({
            acceptAllDevices: true,
            optionalServices: ['0000fe40-cc7a-482a-984a-7f2ed5b3e58f'] // Required to access service later.
        })
        .then(device => {
            // Human-readable name of the device.
            console.log(device.name);

            currentDevice = device;

            // Set up event listener for when device gets disconnected.
            device.addEventListener('gattserverdisconnected', handleAirtameDeviceServerDisconnected);

            // Attempts to connect to remote GATT Server. 00001800-0000-1000-8000-00805f9b34fb
            return device.gatt.connect();
        })
        .then(server => {
            // Getting Battery Service…
            return server.getPrimaryService('0000fe40-cc7a-482a-984a-7f2ed5b3e58f');
        })
        .then(service => {
            // Getting Battery Level Characteristic…
            console.log(`Reading service ${service.uuid}`);
            return service.getCharacteristic('0000fe43-8e22-4541-9d4c-21edae82ed19');
        })
        .then(characteristic => characteristic.startNotifications())
        .then(characteristic => {
            // Reading Battery Level…
            console.log(`Reading characteristic ${characteristic.uuid}`);
            // Set up event listener for when characteristic value changes.
            characteristic.addEventListener('characteristicvaluechanged',
                handleBleCustomValueChanged);
            return characteristic.readValue();
        })
        .then(value => {
            temperatureValue = humidityValue = CO2LevelValue = value.getUint8(0);
            console.log(`Characteristic value is ${value.getUint8(0)}`);
        })
        .catch(error => { console.error(error); });


    if (currentDevice != null && currentDevice.gatt.connected) {
        updateDashboard();
    }

}